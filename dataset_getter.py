import os
import sys
import requests
import tarfile
import gzip
import numpy as np
import pickle as cPickle
import matplotlib.pyplot as plt

"""
TODO:
- download by args - specify the dataset to be downloaded
- use h5py for storage
"""

def download_file(url, file_path):
    if not os.path.exists(file_path):
        r = requests.get(url, allow_redirects=True)
        open(file_path, 'wb').write(r.content)

def unpack_targz(file_path, unpack_path):
    if not os.path.exists(unpack_path):
        with tarfile.open(file_path, 'r:gz') as t:
            t.extractall(path=unpack_path)

def unpack_gzip(file_path, unpack_path):
    if not os.path.exists(unpack_path):
        with gzip.GzipFile(file_path, 'rb') as t:
            s = t.read()
            with open(unpack_path, 'wb') as ut:
                ut.write(s)

def download_datasets():
    dataset_dir = 'datasets'
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)

    # mnist
    dataset = dataset_dir + '/mnist'
    if not os.path.exists(dataset):
        os.makedirs(dataset)
    download_file('http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz', dataset + '/train-images-idx3-ubyte.gz')
    download_file('http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz', dataset + '/train-labels-idx1-ubyte.gz')
    download_file('http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz', dataset + '/t10k-images-idx3-ubyte.gz')
    download_file('http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz', dataset + '/t10k-labels-idx1-ubyte.gz')
    unpack_gzip(dataset + '/train-images-idx3-ubyte.gz', dataset + '/train-images-idx3-ubyte')
    unpack_gzip(dataset + '/train-labels-idx1-ubyte.gz', dataset + '/train-labels-idx1-ubyte')
    unpack_gzip(dataset + '/t10k-images-idx3-ubyte.gz', dataset + '/t10k-images-idx3-ubyte')
    unpack_gzip(dataset + '/t10k-labels-idx1-ubyte.gz', dataset + '/t10k-labels-idx1-ubyte')

    # fashion-mnist
    dataset = dataset_dir + '/fashion-mnist'
    if not os.path.exists(dataset):
        os.makedirs(dataset)
    download_file('http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-images-idx3-ubyte.gz', dataset + '/train-images-idx3-ubyte.gz')
    download_file('http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-labels-idx1-ubyte.gz', dataset + '/train-labels-idx1-ubyte.gz')
    download_file('http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/t10k-images-idx3-ubyte.gz', dataset + '/t10k-images-idx3-ubyte.gz')
    download_file('http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/t10k-labels-idx1-ubyte.gz', dataset + '/t10k-labels-idx1-ubyte.gz')
    unpack_gzip(dataset + '/train-images-idx3-ubyte.gz', dataset + '/train-images-idx3-ubyte')
    unpack_gzip(dataset + '/train-labels-idx1-ubyte.gz', dataset + '/train-labels-idx1-ubyte')
    unpack_gzip(dataset + '/t10k-images-idx3-ubyte.gz', dataset + '/t10k-images-idx3-ubyte')
    unpack_gzip(dataset + '/t10k-labels-idx1-ubyte.gz', dataset + '/t10k-labels-idx1-ubyte')

    # cifar-10
    dataset = dataset_dir + '/cifar-10'
    if not os.path.exists(dataset):
        os.makedirs(dataset)
    download_file('https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz', dataset + '/cifar-10-python.tar.gz')
    unpack_targz(dataset + '/cifar-10-python.tar.gz', dataset)

    # cifar-100
    dataset = dataset_dir + '/cifar-100'
    if not os.path.exists(dataset):
        os.makedirs(dataset)
    download_file('https://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz', dataset + '/cifar-100-python.tar.gz')
    unpack_targz(dataset + '/cifar-100-python.tar.gz', dataset)


def get_cifar10():
    result = {'train_images': np.empty((50000, 3072)), 'train_labels': [], 'test_images': np.empty((10000, 3072)), 'test_labels': []}
    f = 'datasets/cifar-10/cifar-10-batches-py/'
    files = ['data_batch_1', 'data_batch_2', 'data_batch_3', 'data_batch_4', 'data_batch_5', 'test_batch']
    for i in range(len(files)):
        batch_file = files[i]
        with open(f + batch_file, 'rb') as fn:
            d = cPickle.load(fn, encoding='bytes')
            if batch_file != 'test_batch':
                result['train_images'][i * 10000 : (i + 1) * 10000] = d[b'data']
                result['train_labels'][i * 10000 : (i + 1) * 10000] = d[b'labels']
            else:
                result['test_images'] = d[b'data']
                result['test_labels'] = d[b'labels']
    return result

def get_cifar100():
    result = {'train_images': np.empty((50000, 3072)), 'train_labels_fine': [], 'train_labels_coarse': [],
              'test_images': np.empty((10000, 3072)), 'test_labels_fine': [], 'test_labels_coarse': []}
    with open('datasets/cifar-100/cifar-100-python/train', 'rb') as fn:
        d = cPickle.load(fn, encoding='bytes')
        result['train_images'] = d[b'data']
        result['train_labels_fine'] = d[b'fine_labels']
        result['train_labels_coarse'] = d[b'coarse_labels']

    with open('datasets/cifar-100/cifar-100-python/test', 'rb') as fn:
        d = cPickle.load(fn, encoding='bytes')
        result['test_images'] = d[b'data']
        result['test_labels_fine'] = d[b'fine_labels']
        result['test_labels_coarse'] = d[b'coarse_labels']
    return result

def show_image_3dim(image):
    image = image.reshape(3, 32, 32)
    image = image.transpose([1, 2, 0])
    image = image / 256.0
    plt.imshow(image)
    plt.show()

if __name__ == "__main__":
    print(str(sys.argv))
    download_datasets()
    a = get_cifar10()
    get_cifar100()
