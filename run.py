import torch
from mnist import MNIST
from networks.cnn.lenet import Net as lenet

def mnist_run():
    dataset = MNIST('datasets/mnist')
    train_images, train_labels = dataset.load_training()
    test_images, test_labels = dataset.load_testing()

    net = lenet()

if __name__ == "__main__":
    mnist_run()