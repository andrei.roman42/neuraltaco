# neuralTACO

*Totally Arbitrary COllection*

PyTorch Goals:
 - LeNet5 on cifar, train with different optimizers
 - adversarial net
 - recurrent net
 - autoencoder, variable autoencoder, disentangled variable autoencoder
 - denoising autoencoder
 - sparse autoencoder
 - hopfield network
 - boltzmann machine, restricted boltzmann machine, deep belief network
 - deep q networks
 - actor-critic
 - generative adversarial networks
 - liquid state machine
 - extreme learning machine
 - echo state network
 - neural turing machine
 - sequence to sequence with attention

 The networks are mostly covered here:
 https://towardsdatascience.com/the-mostly-complete-chart-of-neural-networks-explained-3fb6f2367464